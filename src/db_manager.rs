use bson::{bson, doc};
use r2d2::ManageConnection;
use mongodb::{Client, Database, options::{ClientOptions, DatabaseOptions}, error::Error};

/// Struct for managing a pool of MongoDB connections
/// Managed object: `mongodb::Database`
pub struct DbManager {
    client_options: ClientOptions,
    db_name: String,
    db_options: DatabaseOptions,
}

impl DbManager {
    pub fn new(client_options: ClientOptions, db_name: &str, db_options: DatabaseOptions) -> DbManager {
        DbManager {
            client_options, db_options,
            db_name: db_name.to_owned(),
        }
    }

    pub fn from_uri(uri: &str, db_name: &str) -> Result<DbManager, Error> {
        Ok(DbManager {
            client_options: ClientOptions::parse(uri)?,
            db_name: db_name.to_owned(),
            db_options: Default::default(),
        })
    }
}

impl ManageConnection for DbManager {
    type Connection = Database;
    type Error = Error;

    fn connect(&self) -> Result<Database, Error> {
        let client = Client::with_options(self.client_options.clone())?;
        let db = client.database_with_options(&self.db_name, self.db_options.clone());
        Ok(db)
    }

    fn is_valid(&self, db: &mut Database) -> Result<(), Error> {
        db.run_command(doc!{"buildInfo": 1}, None)?;
        Ok(())
    }

    fn has_broken(&self, db: &mut Database) -> bool {
        self.is_valid(db).is_err()
    }
}
