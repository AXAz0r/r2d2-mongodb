//! # r2d2-mongodb
//! A MongoDB adaptor for r2d2 connection pool.
//! # Example
//! ```rust
//! // use r2d2::Pool;
//! use r2d2_mongodb::{MongoClientManager, MongoDbManager};
//!
//! fn main () {
//!     // To manage just the client use:
//!     let manager = MongoClientManager::from_uri("mongodb://localhost:27017/").unwrap();
//!     // To manage a specific database use:
//!     let manager = MongoDbManager::from_uri("mongodb://localhost:27017/", "my_database").unwrap();
//!
//!     // let pool = Pool::builder()
//!     //     .max_size(8)
//!     //     .build(manager)
//!     //     .unwrap();
//!
//!     // ...
//! }
//! ```

mod client_manager;
mod db_manager;

pub use client_manager::ClientManager as MongoClientManager;
pub use db_manager::DbManager as MongoDbManager;
